package com.example.javacv;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.VideoView;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacv.AndroidFrameConverter;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.opencv.core.Mat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        method();

           }

    private void method() {
        try {
            InputStream inputStream = new FileInputStream("/sdcard/video/bb.mp4");

            FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(inputStream);
            OpenCVFrameConverter.ToMat converterToMat = new OpenCVFrameConverter.ToMat();

            grabber.start();

            for (int frameCount = 0; frameCount < grabber.getLengthInFrames();frameCount++) {
                Frame frame = grabber.grabImage();
                Mat mat = converterToMat.convertToOrgOpenCvCoreMat(frame);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (FrameGrabber.Exception e) {
            e.printStackTrace();
        }
    }
}